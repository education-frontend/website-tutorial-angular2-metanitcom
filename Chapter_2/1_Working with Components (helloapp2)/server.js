var expressApp = require("express");
var app = expressApp();

var port = 3000;

app.use(function (req, res, next) {
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
        res.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type, Referrer");
        next();
    })
    .use('/desktop', expressApp.static(__dirname + "/app"))
    .listen(port, function () {
        console.log("Server starter: localhost:" + port);
});